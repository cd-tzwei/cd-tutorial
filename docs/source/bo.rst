Raise Back Office Setup
=======================

Setup BO
--------

Git Repository: http://47.89.53.43/phase2/back_office

Steps (Setup BO)
^^^^^^^^^^^^^^^^^^^^
#. Clone project

    ::

        git clone http://47.89.53.43/phase2/back_office

#. Setup i18n folder

    #. Download i18n folder (Choose One)

        - DB Version: https://bo.the777888.com/langUI

            .. image:: images/download_i18n_db.jpg

        - File Version (Old): https://bo.the777888.com/

            .. image:: images/download_i18n_file.jpg

    #. Place the i18n to ``<project_folder>/src`` folder
    #. Example

        .. image:: images/i18n_path.jpg


#. Configure env & config

    - in ``prelive`` branch

        - renaming ``.env.example`` to ``.env``
        - ``<project_folder>/inc`` folder renaming ``.config.e.php`` to ``.config.php``

    .. tip:: To show hidden file in finder Mac

        Press ``command`` + ``shift`` + ``.``

#. :any:`dovHost`
#. DONE
