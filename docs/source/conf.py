project = 'cd-tutorial'
copyright = '2022, Tzwei'
author = 'Tzwei'
master_doc = 'index'

extensions = [
    'sphinx_rtd_theme',
    'sphinxcontrib.video'
]

html_theme = 'sphinx_rtd_theme'