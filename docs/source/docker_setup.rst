Localhost Setup
===============

Setup Docker
------------

Git Repository: http://47.89.53.43/tzwei/cd-docker

Prerequisite
^^^^^^^^^^^^
    - Git: https://git-scm.com/downloads
    - Docker Windows: https://docs.docker.com/desktop/windows/install/
    - Docker Mac: https://docs.docker.com/desktop/mac/install/

Steps (Setup Docker)
^^^^^^^^^^^^^^^^^^^^

#. Open terminal

#. Git clone the project into a directory e.g., C:

    ::

        git clone http://47.89.53.43/tzwei/cd-docker

#. Init docker (copy env and configs automatically), Simply execute at the same directory

    - Mac

    ::

        ./init.sh

    - Windows

    ::

        ./init.bat

#. Go env and change your environment accordingly. Most Important is ``WORK_DIR``

    ::

        WORK_DIR-<Work_Directory>

    Example for ``WORK_DIR``

            - Mac

            ::

                WORK_DIR-~/htdocs/

            - Windows

            ::

                WORK_DIR-D:/htdocs/

    The real path in Docker will become ``/var/www/html/``

    #. Execute the following command to start Docker

    ::

        docker compose up -d

.. _dovHost:

Setup vHost
-----------

vHost is to give the project a domain for local usage.

    - When localhost is done setup, every project is under a folder, so the project URL is like:

    ::

        http://localhost/project1/index.html

    - After vHost will be like:

    ::

        http://project1.dv

Steps (Setup vHost)
^^^^^^^^^^^^^^^^^^^

#. Open hosts file

    - Mac: Open a terminal and execute

    ::

        sudo nano /etc/hosts

    - Windows: Open text editor with Administrator

    ::

        1. Press the Windows key.
        2. Type Notepad in the search field.
        3. In the search results, right-click Notepad and select Run as administrator.
        4. From Notepad, open the following file:  c:\Windows\System32\Drivers\etc\hosts

    .. video:: https://gitlab.com/cd-tzwei/cd-tutorial/-/raw/main/docs/source/videos/openHosts.mp4
        :width: 600
        :height: 300

#. Append value ``127.0.0.1 project1.dv``

        .. caution::
            - Top level domain recomended to use ``.dv``
            - ``.local`` / ``.test`` / ``.dev`` are reserved top level domain by DNS, if use it will causing problem like very slow or unable to access

        .. tip::
            - If want to have multiple vHost domain, write the line like below will be faster loading

            ::

                127.0.0.1 project1.dv project2.dv project3.dv

#. Save **hosts** file

#. Go to Docker config folder ``/config/extra-vhost.conf`` add vHost config for Apache config

    ::

        <VirtualHost *:80>
            ServerName project1.dv
            ServerAdmin webmaster@localhost
            DocumentRoot /var/www/html/project1
        </VirtualHost>

    .. caution::

        ``DocumentRoot`` is the folder path inside the docker container

#. Save and DONE
